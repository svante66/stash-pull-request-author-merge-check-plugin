package com.atlassian.stash.plugin.authormergecheck;

import com.atlassian.stash.hook.repository.RepositoryMergeRequestCheck;
import com.atlassian.stash.hook.repository.RepositoryMergeRequestCheckContext;
import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.pull.PullRequestRef;
import com.atlassian.stash.scm.pull.MergeRequest;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.atlassian.stash.user.StashUser;

import javax.annotation.Nonnull;

public class PreventAuthorMergeCheck implements RepositoryMergeRequestCheck {

    private final StashAuthenticationContext authenticationContext;
    private final I18nService i18nService;

    public PreventAuthorMergeCheck(StashAuthenticationContext authenticationContext, I18nService i18nService) {
        this.authenticationContext = authenticationContext;
        this.i18nService = i18nService;
    }

    @Override
    public void check(@Nonnull RepositoryMergeRequestCheckContext context) {
        StashUser currentUser = authenticationContext.getCurrentUser();
        MergeRequest mergeRequest = context.getMergeRequest();
        if (currentUser != null && isAuthor(currentUser, mergeRequest.getPullRequest())) {
            preventMerge(mergeRequest);
        }
    }

    private void preventMerge(MergeRequest mergeRequest) {
        mergeRequest.veto(
                i18nService.getText("stash.plugin.prevent.author.merging.summary", "You cannot merge your own pull requests"),
                i18nService.getText("stash.plugin.prevent.author.merging.details", "Authors are not allowed to merge their own pull requests. " +
                        "You will need to ask another contributor with write access " +
                        "to {0} to merge it instead.",
                        mergeRequest.getPullRequest().getToRef().getDisplayId())
        );
    }

    private static boolean isAuthor(StashUser user, PullRequest pullRequest) {
        return user.equals(pullRequest.getAuthor().getUser());
    }

}
